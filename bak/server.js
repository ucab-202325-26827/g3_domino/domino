const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const jose = require('jose')
require('dotenv').config()
app.use(express.static('js'))
const io = new Server(server, { cors: { origin: "*" } });


const puerto = process.env.PORT;

//const decoded = jose.decodeJwt("asdasd");
//console.log(decoded);rs

class Partida {
    constructor(jwt, jugadores, turno) {
        this.jwt = jwt;
        this.jugadores = [];
        this.turno = turno;
    }

    agregarJugador(username, socketId) {
        this.jugadores.push(new Jugador(username, socketId));
    }
}

class Jugador {
    constructor(username, socketId) {
        this.username = username;
        this.piezas = [];
        this.socketId = socketId;
        this.posicion = null;
    }
}

// var jugadoresPermitidos = ["Gabriel", "JoseEnrique", "J", "Hernan"];
// http://localhost:3000/?username=Gabriel
// http://localhost:3000/?username=Jose Enrique
// http://localhost:3000/?username=J
// http://localhost:3000/?username=Hernan

// var jugadoresPermitidosCheck = [0, 0, 0, 0];
// const piezasRepartidas = [];  // matriz con las piezas repartidas
// piezasRepartidas[0] = [];      // Piezas del jugador 0
// piezasRepartidas[1] = [];      // Piezas del jugador 1
// piezasRepartidas[2] = [];      // Piezas del jugador 2, el compañero
// piezasRepartidas[3] = [];      // Piezas del jugador 3


var partidas = []

app.post('/', (req, res) => {
    let data = req.body;
    res.send('Listo' + JSON.stringify(data));
})



app.get('/', (req, res) => {

    const found = partidas.find(partida => partida.jwt === req.query.token)
    if (!found) {
        const partida = new Partida(req.query.token);
        partidas.push(partida);
    }

    res.sendFile(__dirname + '/DominoCliente.html');

    // var contJugadores = 0;
    // var encontrado = false;
    // for(var i =0 ; i<4 ; i++){
    //     if(jugadoresPermitidos[i] == req.query.username){
    //         jugadoresPermitidosCheck[i]=1;
    //         encontrado = true;
    //         console.log(req.query.username + " se ha conectado");
    //         res.sendFile(__dirname + '/clientico.html');
    //     }
    //     contJugadores+=jugadoresPermitidosCheck[i];
    // }

    // if(!encontrado)
    //     res.sendFile(__dirname + '/error.html');
    // console.log(contJugadores + " jugadores conectado(s)");
    // if(contJugadores == 4) // listo, todos conectados
    //     ComenzarJuego();


});

io.on('connection', (socket) => {

    socket.on("message", (data) => {
        console.log(" Hola " + data);
        var contJugadores = 0;

    });

    //evento mover pieza
    /*
        params:
        token: token de la partida
        jugador: jugador que movio la pieza
        pieza: pieza movida
    */
    socket.on("jugare-pieza", (data) => {
        //actualizar estado de piezas en servidor

        // if (!data.posicion || !data.nombrePieza || !data.culebra) return;
        const partida = partidas[0];
        partida.turno = (partida.turno + 1) % 4;
        io.emit("juega-pieza", {
            ...data,
            turno: partida.turno,
        });

        // const partida = partidas.find(partida => data.token);

        // if (partida) {

        //     //consigue la instancia del jugador que pertenece a la partida y borra la pieza jugada de su lista de piezas
        //     const jugador = partida.jugadores.find(jugador => jugador.posicion === data.jugador.posicion);
        //     const nPieza = jugador.piezas.findIndex(pieza => pieza.nombre === data.pieza);
        //     delete jugador.piezas[nPieza];

        //     //broadcast actualiza a todos los demas jugadores 
        // }

    });



    socket.on("connect-player", (data) => {
        // send a message to the server

        const partida = partidas.find(partida => partida.jwt == data.token)

        if (partida) {
            if (partida.jugadores.length < 4) {
                partida.agregarJugador(data.username, socket.id);
                socket.emit("numero-jugador", partida.jugadores.length);

                if (partida.jugadores.length == 4) {
                    RepartirPiezas(partida);
                    io.emit("start-game", partida);
                    console.log(`empezando ${data.token} partida`);

                }
                console.log(`usuario ${data.username} agregado a partida ${data.token}`);
            }
        }

    });
});

server.listen(puerto, () => {
    console.log('listening on *' + puerto);
});

// Funciones del Juego Domino

class PiezaNN {
    constructor(nombre, pos) {
        this.nombre = nombre;
        this.pos = pos;
    }
}

function RepartirPiezas(partida) {

    const jugadores = partida.jugadores;
    var piezasN = []

    var contPiezas = 0;
    for (let i = 0; i < 7; i++) // Las crea
        for (let j = i; j < 7; j++) {
            var nombre = i.toString() + j.toString();
            var pieza = new PiezaNN(nombre, contPiezas++);
            piezasN.push(pieza);
        }
    for (let i = 0; i < 4; i++)  // Las reparte
        for (let j = 0; j < 7; j++) {
            var np = Math.floor(Math.random() * piezasN.length);
            jugadores[i].piezas.push(piezasN[np]);
            jugadores[i].posicion = i;
            if (piezasN[np].nombre == "66") {
                partida.turno = i; // jugador que inicia la partida (Cochina)
            }
            piezasN.splice(np, 1); // Elimina la piezas repartida
        }
}
